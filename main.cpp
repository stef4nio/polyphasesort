#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <algorithm>
#include <chrono>


using namespace std;
using namespace std::chrono;

template <typename T>
void WriteVector(const vector<T>&, ostream&);

template <typename T>
vector<T> operator -(const vector<T>& a, const vector<T>& b)
{
    if(a.size()!=b.size())
    {
        throw runtime_error("Substracting arrays must be of equal size");
    }
    int operandSize = a.size();
    vector<T> result;

    for(int i = 0;i<operandSize;i++)
    {
        result.push_back(a[i]-b[i]);
    }
    return result;
}

unsigned long GetFileSize(const basic_string<char>& filename)
{
    ifstream input;
    input.open(filename,ios::binary);
    input.ignore(numeric_limits<streamsize>::max());
    unsigned long result = input.gcount();
    input.close();
    return result;
}

int FindMaxElementIndex(const vector<int>& vec)
{
    int maxData = vec[0], maxIndex = 0;
    int vectorSize = vec.size();
    for(int i = 0;i<vectorSize;i++)
    {
        if(vec[i]>maxData)
        {
            maxData = vec[i];
            maxIndex = i;
        }
    }
    return maxIndex;
}

int FindMinElementIndexWithoutZero(const vector<int>& vec)
{
    bool isFirstIteration = true;
    int minData, minIndex;
    int vectorSize = vec.size();
    for(int i = 0;i<vectorSize;i++)
    {
        if(vec[i] != 0)
        {
            if(isFirstIteration)
            {
                isFirstIteration = false;
                minData = vec[i];
                minIndex = i;
            }
            else if(vec[i]<minData)
            {
                minData = vec[i];
                minIndex = i;
            }
        }

    }
    return minIndex;
}

int FindMinElementIndex(const vector<int>& vec)
{
    int minData = vec[0], minIndex = 0;
    int vectorSize = vec.size();
    for(int i = 0;i<vectorSize;i++)
    {
        if(vec[i]<minData)
        {
            minData = vec[i];
            minIndex = i;
        }
    }
    return minIndex;
}

int FindZeroElementIndex(const vector<int>& vec)
{
    int vectorSize = vec.size();
    for(int i = 0;i<vectorSize;i++)
    {
        if(vec[i] == 0)
        {
            return i;
        }
    }
    return -1;
}

unsigned int GetIdealChunkDistribution(unsigned int chunksAmount, unsigned int filesAmount, vector<int>& distribution)
{
    distribution.resize(filesAmount,0);
    distribution[0] = 1;
    unsigned int sum = 1;
    while (sum<chunksAmount)
    {
        int maxElIndex = FindMaxElementIndex(distribution);
        int maxElData = distribution[maxElIndex];
        for(int i = 0;i<filesAmount;i++)
        {
            distribution[i]+=maxElData;
            sum+=maxElData;
        }
        sum-=distribution[maxElIndex];
        distribution[maxElIndex] = 0;
    }
    return sum;
}

void ProceedOneStepForwardInDistribution(vector<int>& distribution)
{
    int maxElIndex = FindMaxElementIndex(distribution);
    int maxElData = distribution[maxElIndex];
    int filesAmount = distribution.size();
    for(int i = 0;i<filesAmount;i++)
    {
        distribution[i]+=maxElData;
    }
    distribution[maxElIndex] = 0;
}

void ProceedOneStepBackwardInDistribution(vector<int>& distribution)
{
    int minElIndex = FindMinElementIndexWithoutZero(distribution);
    int minElData = distribution[minElIndex];
    int filesAmount = distribution.size();
    for(int i = 0;i<filesAmount;i++)
    {
        if(distribution[i]!=0)
        {
            distribution[i] -= minElData;
        }
        else
        {
            distribution[i] += minElData;
        }

    }
    distribution[minElIndex] = 0;
}

void GetChunksDistribution(unsigned int chunksAmount, unsigned int filesAmount, vector<int>& distribution)
{
    unsigned int sum = GetIdealChunkDistribution(chunksAmount,filesAmount,distribution);
/*    while (sum<chunksAmount)
    {
        int maxElIndex = FindMaxElementIndex(distribution);
        int maxElData = distribution[maxElIndex];
        for(int i = 0;i<filesAmount;i++)
        {
            distribution[i]+=maxElData;
            sum+=maxElData;
        }
        sum-=distribution[maxElIndex];
        distribution[maxElIndex] = 0;
    }*/
    if(sum>chunksAmount)
    {
        unsigned int chunksToRemove = (sum-chunksAmount)/(filesAmount-1);
        unsigned int maxElIndex = FindMaxElementIndex(distribution);
        for(int i = 0;i<filesAmount;i++)
        {
            if(distribution[i] == 0 || i == maxElIndex)
            {
                continue;
            }
            distribution[i]-=chunksToRemove;
            sum-=chunksToRemove;
        }

        distribution[maxElIndex] -=(sum-chunksAmount);
    }
}

void CreateFiles(const string& nameFirstPart, const string& extension, unsigned int filesAmount)
{
    for(int i = 0;i<filesAmount;i++)
    {
        ofstream fileCreator;
        string currFileName;
        currFileName.append(nameFirstPart).append(to_string(i)).append(extension);
        fileCreator.open(currFileName);
        fileCreator.close();
    }
}

void SplitFile(const char* filename, unsigned int maxRamSize, unsigned int filesAmount)
{
    ifstream inputFile;
    inputFile.open(filename, ios::binary);
    unsigned int fileSize = GetFileSize(filename);
    unsigned int numbersLeftToRead = fileSize / sizeof(int);
    unsigned int chunksAmount = fileSize/maxRamSize;
    unsigned int chunkSize = maxRamSize/sizeof(int);
    int lastChunkLastNum = INT32_MAX;
    vector<int> distribution;
    GetChunksDistribution(chunksAmount,filesAmount,distribution);
    unsigned int currFile = 0;
    vector<int> chunkData;
    CreateFiles("chunk_",".dat", filesAmount);
    while(numbersLeftToRead)
    {
        int numsRead = 0;
        while(numbersLeftToRead && numsRead++ < chunkSize)
        {
            int currInt;
            inputFile.read((char*)&currInt,sizeof(int));
            chunkData.push_back(currInt);
            numbersLeftToRead--;
        }
        if(numsRead>0)
        {
            sort(chunkData.begin(), chunkData.end());
            fstream vectorOutput;
            while(distribution[currFile] <= 0)
            {
                currFile++;
                lastChunkLastNum = INT32_MAX;
            }
            vectorOutput.open("chunk_" + to_string(currFile) + ".dat", ios::binary|ios::in|ios::out|ios::ate);
            distribution[currFile]--;
            if(chunkData[0]>lastChunkLastNum)
            {
                chunkData.insert(chunkData.begin()+1,lastChunkLastNum);
                vectorOutput.seekp(-sizeof(int),ios_base::cur);
            }
            lastChunkLastNum = chunkData.back();
            WriteVector(chunkData, vectorOutput);
            vectorOutput.close();
            chunkData.clear();
        }
    }
    inputFile.close();
}


unsigned int GetVectorSum(const vector<int>& vec)
{
    unsigned int result = 0;
    for(int val:vec)
    {
        if(val>=0)
        {
            result += val;
        }
    }
    return result;
}

bool CheckIfFileIsSorted(const basic_string<char, char_traits<char>, allocator<char>>& filepath)
{
    ifstream testStream;
    testStream.open(filepath,ios::binary);
    int number;
    int prevNum = 0;
    while(testStream.read(reinterpret_cast<char*>(&number),sizeof(int)))
    {
        if(prevNum>number)
        {
            return false;
        }
        prevNum = number;
    }
    return true;
}

void MergeFile(int filesAmount, int chunksAmount)
{
    vector<int> currentIdealDistribution;
    GetIdealChunkDistribution(chunksAmount,filesAmount,currentIdealDistribution);
    ProceedOneStepBackwardInDistribution(currentIdealDistribution);
    vector<int> currentDistribution;
    GetChunksDistribution(chunksAmount,filesAmount,currentDistribution);
    vector<int> delta = currentDistribution-currentIdealDistribution;
    fstream fileToMergeInto;
    vector<ifstream> filesToMergeFrom(filesAmount);
    vector<int> firstElementsFromFiles(filesAmount);
    vector<int> numbersLeft(filesAmount);
    int zeroIndex = -1;
    int prevChunkLastNum = INT32_MAX;
    bool isNewChunk = true;

    while(GetVectorSum(currentDistribution)>1)
    {
        zeroIndex = FindZeroElementIndex(currentDistribution);

        for (int i = 0; i < filesAmount; i++)
        {
            if (i != zeroIndex)
            {
                if (!filesToMergeFrom[i].is_open())
                {
                    numbersLeft[i] = GetFileSize("chunk_" + to_string(i) + ".dat")/sizeof(int);
                    filesToMergeFrom[i].open("chunk_" + to_string(i) + ".dat", ios::binary);
                    int currEl;
                    filesToMergeFrom[i].read((char *) &currEl, sizeof(int));
                    firstElementsFromFiles[i] = currEl;
                }
            }
            else
            {
                filesToMergeFrom[i].close();
                firstElementsFromFiles[i] = -1;
            }
        }

        fileToMergeInto.open("chunk_" + to_string(zeroIndex) + ".dat", ios::binary | ios::out | ios::trunc);

        do
        {
            vector<bool> isChunkSatisfied(filesAmount,true);

            for (int i = 0; i < filesAmount; i++)
            {
                if (currentDistribution[i]!=0)
                {
                    isChunkSatisfied[i] = delta[i] <= 0 || numbersLeft[i]==0;
                }
            }

            do
            {
                int minIndex;
                int minData;

                do
                {
                    bool isFirstIteration = true;

                    for (int i = 0; i < filesAmount; i++)
                    {
                        if (!isChunkSatisfied[i])
                        {
                            if (isFirstIteration)
                            {
                                minIndex = i;
                                minData = firstElementsFromFiles[i];
                                isFirstIteration = false;
                            }
                            else
                            {
                                if (firstElementsFromFiles[i] < minData)
                                {
                                    minData = firstElementsFromFiles[i];
                                    minIndex = i;
                                }
                            }
                        }
                    }

                    if(isNewChunk&& minData>prevChunkLastNum)
                    {
                        fileToMergeInto.seekp(-sizeof(int),ios_base::cur);
                        fileToMergeInto.write(reinterpret_cast<char *> (&minData), sizeof(int));
                        fileToMergeInto.write(reinterpret_cast<char *> (&prevChunkLastNum), sizeof(int));
                    }
                    else
                    {
                         fileToMergeInto.write(reinterpret_cast<char *> (&minData), sizeof(int));
                    }
                    isNewChunk = false;

                    numbersLeft[minIndex]--;
                    numbersLeft[zeroIndex]++;

                    if(numbersLeft[minIndex] == 0)
                    {
                        break;
                    }

                    int buffer;
                    filesToMergeFrom[minIndex].read(reinterpret_cast<char *> (&buffer), sizeof(int));
                    firstElementsFromFiles[minIndex] = buffer;
                } while (minData <= firstElementsFromFiles[minIndex]);

                if(numbersLeft[minIndex] == 0)
                {
                    currentDistribution[minIndex] = 0;
                    delta[minIndex] = 0;
                }
                else
                {
                    currentDistribution[minIndex]--;
                    delta[minIndex]--;
                }
                isChunkSatisfied[minIndex] = true;
                prevChunkLastNum = minData;

            } while (!all_of(isChunkSatisfied.begin(), isChunkSatisfied.end(), [](bool data)
            { return data; }));

            currentDistribution[zeroIndex]++;
            isNewChunk = true;

        } while (GetVectorSum(delta) > 1);

        fileToMergeInto.close();
        ProceedOneStepBackwardInDistribution(currentIdealDistribution);
        delta = currentDistribution-currentIdealDistribution;
    }
}

void GenerateRandomFile(const char* filename, const unsigned long& fileSize)
{
    srand(1448);
    ofstream out;
    out.open(filename,ios::binary);
    int num;
    unsigned long numAmount = fileSize/ sizeof(int);
    for(long i = 0;i<numAmount;i++)
    {
        num = rand() % 10000;
        out.write(reinterpret_cast<const char *>(&num), sizeof(num));
    }
    out.close();
}

void GenerateIdealFile(const char* filename, const unsigned long& fileSize)
{
    ofstream out;
    out.open(filename,ios::binary);
    unsigned long numAmount = fileSize/ sizeof(int);
    unsigned long i;
    for(i = numAmount;i>0;i--)
    {
        out.write(reinterpret_cast<const char *>(&i), sizeof(int));
    }
    out.close();
}

template <typename T>
void WriteVector(const vector<T>& vector, ostream& stream)
{
    for(T el:vector)
    {
        stream.write(reinterpret_cast<char*>(&el),sizeof(T));
    }
}

int main()
{
    int numbersAmount = 1000000;
    int chunkSize = 100;
    int filesAmount = 5;
    int chunksAmount = numbersAmount/chunkSize;
    GenerateRandomFile("test.dat", numbersAmount * sizeof(int));
    auto start = steady_clock::now();
    SplitFile("test.dat", chunkSize * sizeof(int), filesAmount);
    MergeFile(filesAmount, chunksAmount);
    auto end = steady_clock::now();
    cout<<(CheckIfFileIsSorted("chunk_0.dat")?"sorted":"unsorted")<<endl;
    std::cout << "end sort " << (duration_cast<milliseconds>(end - start).count()) * 0.001 << " sec" << std::endl;
    return 0;
}
